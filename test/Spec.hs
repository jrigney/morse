{-# LANGUAGE DeriveGeneric #-}

module Main where

import qualified Data.Map as M
import Morse
import Test.QuickCheck
import Test.QuickCheck.Gen (oneof)
import GHC.Generics

main :: IO ()
main = quickCheck prop_thereAndBackAgain

allowedChars :: [Char]
allowedChars = M.keys letterToMorse

allowedMorse :: [Morse]
allowedMorse = M.elems letterToMorse

charGen :: Gen Char
charGen = elements allowedChars

morseGen :: Gen Morse
morseGen = elements allowedMorse

prop_thereAndBackAgain :: Property
prop_thereAndBackAgain =
  forAll charGen (\c -> ((charToMorse c) >>= morseToChar) == Just c)


-- Babby's First Arbitrary

data Trivial = Trivial deriving (Eq, Show)

trivialGen :: Gen Trivial
trivialGen = return Trivial

-- set the default gen to trivialGen
instance Arbitrary Trivial where
    arbitrary = trivialGen

-- Identity
-- sample (arbitrary :: Gen (Identity String))
-- sample identityGenInt
data Identity a = Identity a deriving (Eq, Show) 

identityGen :: Arbitrary a => Gen (Identity a)
identityGen = do
    a <- arbitrary
    return (Identity a)

-- set the default gen to identityGen
instance Arbitrary a => Arbitrary (Identity a) where
    arbitrary = identityGen

identityGenInt :: Gen (Identity Int)
identityGenInt = identityGen

-- Arbitrary Products
-- sample (arbitrary :: Gen (Pair Int String))
data Pair a b = Pair a b deriving (Eq, Show)

pairGen :: (Arbitrary a, Arbitrary b) => Gen (Pair a b)
pairGen = do
    a <- arbitrary
    b <- arbitrary
    return (Pair a b)

instance (Arbitrary a , Arbitrary b) => Arbitrary (Pair a b) where
    arbitrary = pairGen

pairGenIntString :: Gen (Pair Int String)
pairGenIntString = pairGen

-- Arbitrary Sums
-- sample (arbitrary :: Gen (Sum Int String))
data Sum a b =
    First a
    | Second b
    deriving (Eq, Show)

-- equal odds for each
sumGenEqual :: (Arbitrary a, Arbitrary b) => Gen (Sum a b)
sumGenEqual = do
    a <- arbitrary
    b <- arbitrary
    oneof[return $ First a,
          return $ Second b]

instance (Arbitrary a, Arbitrary b ) => Arbitrary (Sum a b) where
    arbitrary = sumGenEqual

sumGenCharInt :: Gen (Sum Char Int)
sumGenCharInt = sumGenEqual

--sample (sumGenFirstPls :: Gen (Sum Char Int))
sumGenFirstPls :: (Arbitrary a, Arbitrary b) => Gen (Sum a b)
sumGenFirstPls = do
    a <- arbitrary
    b <- arbitrary
    frequency [(10, return $ First a),
               (1, return $ Second b)]

sumGenCharIntFirst :: Gen (Sum Char Int)
sumGenCharIntFirst = sumGenFirstPls

-- CoArbitrary
data Bool' = True'
           | False'
           deriving (Generic)

instance CoArbitrary Bool'

trueGen :: Gen Int
trueGen = coarbitrary True' arbitrary
